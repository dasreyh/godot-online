using Godot;
using System;

public class Ball : Area2D
{
    const int DEFAULT_SPEED = 80;

    Vector2 direction = new Vector2(1, 0);
    float ballSpeed = DEFAULT_SPEED;
    bool stopped = false;
    private Vector2 screenSize;

    public override void _Ready()
    {
        screenSize = GetViewport().Size;
    }

    [Sync]
    public void ResetBall(bool forLeft)
    {
        Position = screenSize / 2;
        if (forLeft)
            direction = new Vector2(-1, 0);
        else
            direction = new Vector2(1, 0);

        ballSpeed = DEFAULT_SPEED;
    }

    [Sync]
    public void Stop()
    {
        stopped = true;
    }

    public override void _Process(float delta)
    {
        if (!stopped)
            Translate(direction * ballSpeed * delta);

        var ballPos = Position;
        if ((ballPos.y < 0 && direction.y < 0) || (ballPos.y > screenSize.y && direction.y > 0))
            direction.y = -direction.y;

        if (IsNetworkMaster())
            if (ballPos.x < 0)
            {
                GetParent().Rpc("UpdateScore", false);
                Rpc("ResetBall", false);
            }
            else
            if (ballPos.x > screenSize.x)
            {
                GetParent().Rpc("UpdateScore", false);
                Rpc("ResetBall", true);
            }
    }

    [Sync]
    public void Bounce(bool left, int random)
    {
        if (left)
            direction.x = Mathf.Abs(direction.x);
        else
            direction.x = -Mathf.Abs(direction.x);
        ballSpeed *= 1.1f;
        direction.y = random * 2.0f - 1;
        direction = direction.Normalized();
    }
}
