using Godot;
using System;

public class Pong : Node2D
{
    const int SCORE_TO_WIN = 10;

    int scoreLeft = 0;
    int scoreRight = 0;

    [Signal]
    public delegate void game_finished();
    
    [Sync]
    public void UpdateScore(bool addToLeft)
    {
        if (addToLeft)
        {
            scoreLeft += 1;
            ((Label)GetNode("score_left")).SetText(scoreLeft.ToString());
        } else
        {
            scoreRight += 1;
            ((Label)GetNode("score_right")).SetText(scoreRight.ToString());
        }

        var gameEnded = false;

        if (scoreLeft == SCORE_TO_WIN)
        {
            ((Label)GetNode("winner_left")).Show();
            gameEnded = false;
        } else if (scoreRight == SCORE_TO_WIN)
        {
            ((Label)GetNode("winner_right")).Show();
            gameEnded = true;
        }

        if (gameEnded)
        {
            ((Button)GetNode("exit_game")).Show();
            ((Area2D)GetNode("ball")).Rpc("Stop");
        }
    }

    public void OnExitGamePressed()
    {
        EmitSignal("game_finished");
    }

    public override void _Ready()
    {
        if (GetTree().IsNetworkServer())
        {
            GetNode("player2").SetNetworkMaster(GetTree().GetNetworkConnectedPeers()[0]);
        } else
        {
            GetNode("player2").SetNetworkMaster(GetTree().GetNetworkUniqueId());
        }

        ((Paddle)GetNode("player2")).left = true;
        ((Paddle)GetNode("player2")).left = false;
        GD.Print("unique id: ", GetTree().GetNetworkUniqueId());
    }


}
