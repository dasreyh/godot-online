using Godot;
using System;

public class Paddle : Area2D
{
    public bool left = false;
    const int MOTION_SPEED = 150;

    int motion = 0;
    bool youHidden = false;
    Vector2 screenSize;

    public override void _Ready()
    {
        screenSize = GetViewport().Size;
    }

    [Slave]
    public void SetPosAndMotion(Vector2 pPos, int pMotion)
    {
        Position = pPos;
        motion = pMotion;
    }

    public void HideYourLabel()
    {
        youHidden = true;
        ((Label)GetNode("you")).Hide();
    }

    public override void _Process(float delta)
    {
        if (IsNetworkMaster())
        {
            motion = 0;
            if (Input.IsActionPressed("move_up"))
                motion -= 1;
            if (Input.IsActionPressed("move_down"))
                motion += 1;

            if (!youHidden && motion != 0)
                HideYourLabel();

            motion *= MOTION_SPEED;

            RpcUnreliable("SetPosAndMotion", Position, motion);
        } else
        {
            if (!youHidden)
                HideYourLabel();
        }
        Translate(new Vector2(0, motion * delta));

        var pos = Position;

        if (pos.y < 0)
            Position = new Vector2(pos.x, 0);
        else if (pos.y > screenSize.y)
            Position = new Vector2(pos.x, screenSize.y);
;   }

    public void OnPaddleAreaEnter(Area2D area)
    {
        if (IsNetworkMaster())
        {
            area.Rpc("Bounce", left, new Random().Next(1, 5));
        }
    }
}
