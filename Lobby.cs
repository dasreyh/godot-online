using Godot;
using System;

public class Lobby : Control
{

    const int DEFAULT_PORT = 8910;

    public override void _Ready()
    {
        GetTree().Connect("network_peer_connected", this, "PlayerConnected");
        GetTree().Connect("network_peer_disconnected", this, "PlayerDisconnected");
        GetTree().Connect("connected_to_server", this, "ConnectedOk");
        GetTree().Connect("connection_failed", this, "ConnectedFail");
        GetTree().Connect("server_disconnected", this, "ServerDisconnected");
    }

    public void PlayerConnected(int id)
    {
        var pongScene = (PackedScene)ResourceLoader.Load("res://pong.tscn");
        var pong = pongScene.Instance();
        pong.Connect("game_finished", this, "EndGame", null, (int)ConnectFlags.Deferred);

        GetTree().GetRoot().AddChild(pong);
        Hide();
    }

    public void PlayerDisconnected(int id)
    {
        if (GetTree().IsNetworkServer())
            EndGame("Client Disconnected");
        else
            EndGame("Server Disconnected");
    }

    public void ConnectedOk()
    {

    }

    public void ConnectedFail()
    {
        SetStatus("Couldnt Connect", false);

        GetTree().SetNetworkPeer(null);

        ((Button)GetNode("panel/join")).SetDisabled(false);
        ((Button)GetNode("panel/host")).SetDisabled(false);
    }

    public void ServerDisconnected()
    {
        EndGame("Server disconnected");
    }

    public void EndGame(string err)
    {
        if (HasNode("/root/pong"))
        {
            GetNode("/root/pong").Free();
            Show();
        }

        GetTree().SetNetworkPeer(null);

        ((Button)GetNode("panel/join")).SetDisabled(false);
        ((Button)GetNode("panel/host")).SetDisabled(false);

        
    }

    public void SetStatus(string text, bool isOk)
    {
        if (isOk)
        {
            ((Label)GetNode("panel/status_ok")).SetText(text);
            ((Label)GetNode("panel/status_fail")).SetText("");
        } else
        {
            ((Label)GetNode("panel/status_ok")).SetText("");
            ((Label)GetNode("panel/status_fail")).SetText(text);
        }
    }

    public void OnHostPressed()
    {
        var host = new NetworkedMultiplayerENet();
        host.SetCompressionMode(NetworkedMultiplayerENet.CompressionModeEnum.RangeCoder);
        Error err = host.CreateServer(DEFAULT_PORT, 1);
        if (err != Error.Ok)
        {
            SetStatus("Can't host, address in use.", false);
            return;
        }

        GetTree().SetNetworkPeer(host);
        ((Button)GetNode("panel/join")).SetDisabled(true);
        ((Button)GetNode("panel/host")).SetDisabled(true);
        SetStatus("Waiting for player..", true);
    }

    public void OnJoinPressed()
    {
        var address = (LineEdit)GetNode("panel/address");
        var ip = address.GetText();

        if (!ip.IsValidIpAddress())
        {
            SetStatus("IP addresss is invalid", false);
            return;
        }

        var host = new NetworkedMultiplayerENet();
        host.SetCompressionMode(NetworkedMultiplayerENet.CompressionModeEnum.RangeCoder);
        host.CreateClient(ip, DEFAULT_PORT);
        GetTree().SetNetworkPeer(host);
        SetStatus("Connecting...", true);
    }
}
